db.rooms.insertOne({
    "name": "single",
    "accomodates": 2,
    "price": 1000,
    "description": "A simple room with all the basic necessities",
    "rooms_available": 10
});

db.rooms.insertMany([
    {
        "name": "double",
        "accomodates": 4,
        "price": 4000,
        "description": "A room with a queen sized bed perfect for a simple getaway",
        "rooms_available": 15,
        "isAvailable": false
    },
    {
        "name": "queen",
        "accomodates": 4,
        "price": 4000,
        "description": "A room with a queen sized bed perfect for a simple getaway",
        "rooms_available": 15,
        "isAvailable": false
    },
    {
        "name": "king",
        "accomodates": 4,
        "price": 4000,
        "description": "A room with a queen sized bed perfect for a simple getaway",
        "rooms_available": 15,
        "isAvailable": false
    }
]);

db.rooms.find({ "name": "double" });

db.rooms.updateOne(
    {
        "name": "queen",
    },
    {
        $set: {
            "rooms_available": 0,
        }
    }
)

db.rooms.deleteMany(
    {
        "rooms_available": 0,
    }
);